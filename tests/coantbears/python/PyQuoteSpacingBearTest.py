from coantbears.python.PyQuoteSpacingBear import PyQuoteSpacingBear
from coalib.testing.LocalBearTestHelper import verify_local_bear

implicitStringConcatGoodFile = """
maintainer_email=('lasse.schuirmann@gmail.com, '
                  'fabian@neuschmidt.de, '
                  'makman@alice.de')
"""

implicitStringConcatBadFile = """
maintainer_email=('lasse.schuirmann@gmail.com, '
                  'fabian@neuschmidt.de, '
                  'makman@alice.de ')
"""

useSlashBadFile = """query = 'SELECT action.descr as "action", '\
    'role.id as role_id,'\
    'role.descr as role '
"""

useSlashGoodFile = """query = 'SELECT action.descr as "action", '\
    'role.id as role_id,'\
    'role.descr as role'
"""

mixedQuoteGood = """
a_str = ('this is single quote line '
         "this is double quote with no trailing space")
"""
mixedQuoteBad = """
a_str = ('this is single quote line '
         "this is double quote with a trailing space ")
"""


implicitConcatSpacingTest = verify_local_bear(
                                PyQuoteSpacingBear,
                                valid_files=(implicitStringConcatGoodFile,
                                             mixedQuoteGood,
                                             useSlashGoodFile,
                                             ),
                                invalid_files=(implicitStringConcatBadFile,
                                               mixedQuoteBad,
                                               useSlashBadFile,
                                               ),
                                )
