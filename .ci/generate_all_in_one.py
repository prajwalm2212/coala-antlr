def to_str(strings):
    result = ''
    for string in strings:
        result = result+string
    return result


lexer = ''
parser = ''
listener = ''
visitor = ''

with open('Python3/Python3Lexer.py', 'r') as lexer_file:
    lexer = lexer_file.readlines()

with open('Python3/Python3Parser.py', 'r') as parser_file:
    parser = parser_file.readlines()

with open('Python3/Python3Listener.py', 'r') as listener_file:
    listener = listener_file.readlines()

with open('Python3/Python3Visitor.py', 'r') as visitor_file:
    visitor = visitor_file.readlines()

with open('../coantlib/coantparsers/Python3.py', 'w') as all_in_one_file:
    all_in_one_file.write(to_str(lexer[:9]))
    all_in_one_file.write(to_str(parser[6:]))
    all_in_one_file.write(to_str('LanguageParser = Python3Parser'))
    all_in_one_file.write(to_str(lexer[16:]))
    all_in_one_file.write(to_str(listener[6:]))
    all_in_one_file.write(to_str(visitor[6:-1]))

with open('xml/XMLLexer.py', 'r') as lexer_file:
    lexer = lexer_file.readlines()

with open('xml/XMLParser.py', 'r') as parser_file:
    parser = parser_file.readlines()

with open('xml/XMLParserListener.py', 'r') as listener_file:
    listener = listener_file.readlines()

with open('xml/XMLParserVisitor.py', 'r') as visitor_file:
    visitor = visitor_file.readlines()

with open('../coantlib/coantparsers/XML.py', 'w') as all_in_one_file:
    all_in_one_file.write(to_str(parser))
    all_in_one_file.write(to_str(lexer[6:]))
    all_in_one_file.write(to_str(listener[6:]))
    all_in_one_file.write(to_str(visitor[6:-1]))
