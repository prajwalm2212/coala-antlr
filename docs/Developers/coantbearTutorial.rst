coantbear Tutorial
==================

Welcome. This tutorial aims to show you how to use ``coantlib`` i.e an antlr
based library to enable writing native bears for coala that can leverage the
power of `ANTLRv4 <http://www.antlr.org/>`__.

We will build a camelCase enforcing bear for function names in this tutorial.


Why is This Useful?
-------------------

coantlib takes care of very high level tasks and all the bear needs to do is
focus on the core logic. Thus the library takes care of curating results into
appropriate data structures and the bear writer can use these results directly
without worrying about traversing the ANTLR's parse tree or collecting results.


What are the extra dependencies?
--------------------------------

The only dependency that library requires additionally is 
`antlr4-python3-runtime 
<https://github.com/antlr/antlr4/tree/master/runtime/Python3/src/
antlr4>`_. This gets already installed with ``coala-antlr``. Also the main
``coala`` package is required for using the ``coantbears``.

.. note::

    A newer version update may break the helper functions provided by the 
    library, so it's advised to use the version specified by ``coala-antlr``.


Writing the Bear
----------------

To write a ``coantbear`` you need to derive the bear from ``ASTBear`` provided
by ``coantlib``. Additionally, the first statement in your bear's ``run``
method should be a call to ``ASTBear``'s ``run`` method to initialise parse
tree with source code.

This initialisation will set an instance variable ``self.walker`` which would
contain a ``walker`` of an appropriate type depending on the source language.

All library functions available for that grammar can be availed using this
``walker``.

.. code:: python
   
    from coantlib.ASTBear import ASTBear

    CamelFunctionBear (ASTBear):

        def run(self, filename, file):
            super(CamelFunctionBear, self).run(filename, file)
            # now self.walker is set to a walker from coantlib

Accessing walker functions
--------------------------

Walker functions can be accessed just like any method of a class using the
class's instance.
For e.g, lets say we want a list of all the function's names and enforce a
camelCase convention for a Python3 source code.
Then all our bear needs to do is, call the ``get_functions`` function from
``coantlib.coantwalkers.Python3Walker``.

.. note::

   The walker was selected by the library automatically.
   If none can be identified, an error is returned.

   For selecting a walker `LANGUAGES` variable is the recommended way, since
   automatic detection depends upon the extension of the file, and if none is
   found then the bear will throw an error.
   e.g ::

      XLanguageBear (ASTBear):
          ...
          LANGUAGES= = {'XLanguage'}
          ...

.. code:: python

    from coantlib.ASTBear import ASTBear

    CamelFunctionBear (ASTBear):

       LANGUAGES = {'Python3'}

       def run(self, filename, file):
           super().run(filename, file)
           # now self.walker is set to a walker from coantlib
           
           function_list = self.walker.get_functions()
           # get_functions() is our helper function from coantlib

           for function in function_list:
              current_name = function.text # this is the property of NodeData

              if current_name.find('_') != -1:
                  yield Result.from_values(
                              origin = self,
                              message = "Not camel case !",
                              file = filename,
                              line = function.lineNo,
                              column = function.columnNo)

And that's done !
We don't have to worry about fetching the function names from the source code.


What if the walker doesn't suite my needs ?
-------------------------------------------

We would love to have a function that meets your requirements, and if there
isn't one, please raise an issue and help us include it into ``coantlib``.

It will also help some future fellow bear writer !
