echo "Installing JAVA"
apt-get update
apt-get install -y software-properties-common
apt-get install -y ca-certificates-java
update-ca-certificates -f
apt-get update
add-apt-repository ppa:openjdk-r/ppa -y
apt-get install -y openjdk-8-jre
echo "Changing Directory"
cd /usr/local/lib
echo "Downloading ANTLR"
curl -O https://www.antlr.org/download/antlr-4.7.1-complete.jar
