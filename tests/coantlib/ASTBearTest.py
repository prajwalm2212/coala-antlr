from coantlib.ASTBear import ASTBear
from coalib.settings.Section import Section
from queue import Queue


file1 = ('import x\n',
         'print("sample Python file")',
         )


class ASTBearTest(object):
    def setup(self):
        self.msg_queue = Queue()
        self.section = Section('')
        self.uut = ASTBear(self.section, self.msg_queue)

    def test_walker_auto(self):
        self.uut.run('file1.py', file1)
        assert self.uut.walker is not None

    def test_walker_py(self):
        class TestBear(ASTBear):
            LANGUAGES = {'Python 3'}

            def run(self, filename, file):
                super().run(filename, file)
        temp = TestBear(Section(''), Queue(''))
        temp.run('file1', file1)
        assert temp.walker is not None
