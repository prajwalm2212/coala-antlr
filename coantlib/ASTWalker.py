"""ASTWalker module."""

from coalib.bearlib.languages.Language import Language
from coantlib.ASTLoader import ASTLoader
from coantlib.mapper import parser_map
from importlib import import_module


class ASTWalker():
    """Base class for methods on walker."""

    treeRoot = None
    tree = None

    @staticmethod
    def get_walker(lang):
        """Resolve walkers depending on ``lang``."""
        y = Language[lang].__class__
        x = import_module(parser_map[y][2])  # third element is walker module
        return getattr(x, parser_map[y][3])  # fourth element is class name

    def __init__(self, file, filename, lang='auto'):
        """Load the Parse Tree of file into a class variable."""
        self.tree, entrypoint = ASTLoader.loadFile(file, filename, lang)
        self.treeRoot = entrypoint()
