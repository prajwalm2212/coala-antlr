"""coantlib package."""

from os.path import join, dirname


VERSION_FILE = join(dirname(__file__), 'VERSION')


def get_version():
    """Return current version of library based on ``VERSION`` file."""
    with open(VERSION_FILE, 'r') as ver:
        return ver.readline().strip()


VERSION = get_version()
__version__ = VERSION
