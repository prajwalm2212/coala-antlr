"""
Stores a map for generalizing grammar entrypoints.

Structure of parser_map::

    {
        Language : (
            Lexer,
            Parser,
            Walker string,
            Walker class name,
            grammar entrypoint rule,
        )
    }
"""

from coalib.bearlib.languages.definitions import Python
from coalib.bearlib.languages.definitions import XML
from coantlib.coantparsers.Python3 import Python3Lexer, Python3Parser
from coantlib.coantparsers.XML import XMLLexer, XMLParser


parser_map = {
    Python.Python: (Python3Lexer,
                    Python3Parser,
                    'coantlib.coantwalkers.Py3Walker',
                    'Py3Walker',
                    'file_input',
                    ),
    XML.XML: (XMLLexer,
              XMLParser,
              'coantlib.coantwalkers.XMLWalker',
              'XMLWalker',
              'document',
              ),
}
